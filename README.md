# Kotlin-spring-examples
A set of examples to demonstrate the usage of Kotlin with Spring Boot:
* Spring MVC
* Spring WebFlux
* Kotlin Coroutines + WebFlux + classic request handlers
* Kotlin Coroutines + WebFlux + functional router
* Examples of tests and mocks with Mockito-kotlin and MockK, including coroutine tests
